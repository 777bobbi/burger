import React from 'react';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient'
import classes from './Burger.css';

const burger = (props) => {
    // const transformedIngredients = Object.keys(props.ingredients)
    //     .map(igName => {
    //         return [...Array(props.ingredients[igName])].map((_, i) => {
    //             return <BurgerIngredient key={igName + i} type={igName}/>;
    //         });
    //     })
    //     .reduce((arr, el) => {
    //         return arr.concat(el);
    //     });

    let transformedIngredients = [];

    for (let igName in props.ingredients) {
        for (let i = 0; i < props.ingredients[igName]; i++) {
            transformedIngredients.push(<BurgerIngredient key={igName + i} type={igName}/>);
        }
    }

    if (transformedIngredients.length === 0) {
        transformedIngredients = <p>Please start adding ingredients</p>;
    }

    return (
        <div className={classes.Burger}>
            <BurgerIngredient type="bread-top"/>
            {transformedIngredients}
            <BurgerIngredient type="bread-bottom"/>
        </div>
    );
};

export default burger;