import React from 'react';
import cl from './NavigationItem.css';

const navigationItem = (props) => (
    <li className={cl.NavigationItem}>
        <a
            href={props.link}
            className={props.active ? cl.active : null}
        >{props.children}</a>
    </li>
);

export default navigationItem;